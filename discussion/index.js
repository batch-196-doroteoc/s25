console.log("Cami!!!");
//JSON
//JSON or javascript open notation is a popular data format for application to commmunicate with each other
//JSON may llok like a js obj but it is actually a string

/*
	JSON syntax

	{
		"key1": "valueA",
		"Key2": "valueB"


	}

	Keys are wrapped in curly braces
*/

let sample1= `
	{
		"name": "Katniss Everdeen",
		"age": 20,
		"address": {
			"ciy": "Kansas City",
			"state": "Kansas"
		}
	}

	`;
console.log(sample1);

//are we able to turn a JSON back into a JS object?
//JSON.parse() - will return the JSON as a JS object

console.log(JSON.parse(sample1));

//JSON Array is an array of JSON

let sampleArr = `
	[
		{
			"email": "jasonNewsted@gmail.com",
			"password": "iplaybass1234",
			"isAdmin": false
		},

		{
			"email": "larsDrums@gmail.com",
			"password": "metallidMe80",
			"isAdmin": true
		}
	]

`;
console.log(sampleArr);
//Can we use Array Methods on a JSON array?
//no because json is a string
//So what can we do to be able to add more items/objects into our sampleArr?
//Parsed the JSON array to a JS array and saved it in a variable
let parsedSampleArr=JSON.parse(sampleArr);
console.log(parsedSampleArr);

//can we now delete the last item in the JSON array?
console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

//If for example we need to send this data back to the client/front end, it should be in JSON format
//JSON.parse() does not mutate or update the original JSON
//we can actually turn a JS object into a JSON
//JSON.stringify() - will stringify JS objects as JSON

sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

//server

//Database => Server/API (JSON to JS object to process) => sent as JSON => client

//servers communicate via strings

//MinA

//Given a JSON array, process it and convert it to a JS object so we can manipulate the array

//delete the last item in the new array and add a new item in the array

//stringfy the array back in JSON

let jsonArr = `
	[
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	]

`;
console.log(jsonArr);

let parsedJsonArr = JSON.parse(jsonArr);
console.log(parsedJsonArr);

console.log(parsedJsonArr.splice(5,1,"Chickenjoy"));
console.log(parsedJsonArr);

stringJsonArr = JSON.stringify(parsedJsonArr);
console.log(stringJsonArr);